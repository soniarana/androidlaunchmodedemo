package com.demo.launchmodedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sonia on 23/11/16.
 */

public class SingleInstanceActivity extends AppCompatActivity implements View.OnClickListener{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.standard_layout);
        ((TextView)findViewById(R.id.tv_title)).setText("Single Instance");
        findViewById(R.id.btn_single_instance).setOnClickListener(this);
        findViewById(R.id.btn_single_task).setOnClickListener(this);
        findViewById(R.id.btn_single_top).setOnClickListener(this);
        findViewById(R.id.btn_standard).setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_single_instance:
                startActivity(new Intent(SingleInstanceActivity.this,SingleInstanceActivity.class));
                break;
            case R.id.btn_single_top:
                startActivity(new Intent(SingleInstanceActivity.this,SingleTopActivity.class));

                break;
            case R.id.btn_single_task:
                startActivity(new Intent(SingleInstanceActivity.this,SingleTaskActivity.class));

                break;
            case R.id.btn_standard:
                startActivity(new Intent(SingleInstanceActivity.this,StandardActivity.class));

                break;
        }
    }
}
