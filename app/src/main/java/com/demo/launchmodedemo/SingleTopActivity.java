package com.demo.launchmodedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sonia on 23/11/16.
 */

public class SingleTopActivity extends AppCompatActivity implements View.OnClickListener{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.standard_layout);
        ((TextView)findViewById(R.id.tv_title)).setText("Single Top");
        findViewById(R.id.btn_single_instance).setOnClickListener(this);
        findViewById(R.id.btn_single_task).setOnClickListener(this);
        findViewById(R.id.btn_single_top).setOnClickListener(this);
        findViewById(R.id.btn_standard).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_single_instance:
                startActivity(new Intent(SingleTopActivity.this,SingleInstanceActivity.class));
                break;
            case R.id.btn_single_top:
                startActivity(new Intent(SingleTopActivity.this,SingleTopActivity.class));

                break;
            case R.id.btn_single_task:
                startActivity(new Intent(SingleTopActivity.this,SingleTaskActivity.class));

                break;
            case R.id.btn_standard:
                startActivity(new Intent(SingleTopActivity.this,StandardActivity.class));

                break;
        }
    }
}
