package com.demo.launchmodedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by sonia on 23/11/16.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        findViewById(R.id.btn_single_instance).setOnClickListener(this);
        findViewById(R.id.btn_single_task).setOnClickListener(this);
        findViewById(R.id.btn_single_top).setOnClickListener(this);
        findViewById(R.id.btn_standard).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_single_instance:
                startActivity(new Intent(MainActivity.this,SingleInstanceActivity.class));
                break;
            case R.id.btn_single_top:
                startActivity(new Intent(MainActivity.this,SingleTopActivity.class));

                break;
            case R.id.btn_single_task:
                startActivity(new Intent(MainActivity.this,SingleTaskActivity.class));

                break;
            case R.id.btn_standard:
                startActivity(new Intent(MainActivity.this,StandardActivity.class));

                break;
        }
    }
}
